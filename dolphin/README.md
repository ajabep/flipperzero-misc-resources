# Dolphin

Here are picture of what your dolphin does in their spare time.
A lot of directory has not been edit: We need all of them and of their content to compile one of them.


## Compilation

Use the script [`../scripts/assets.py`](../scripts/assets.py).


## Ressources

For more information, please refer https://github.com/flipperdevices/flipperzero-firmware/tree/dev/assets/dolphin/external.
