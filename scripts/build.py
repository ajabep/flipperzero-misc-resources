import argparse
import logging
import os
import shutil
from assets import Main
from pathlib import Path

BADUSB_PLAYER_DIRNAME = "badusb"
SUBGHZ_PLAYER_DIRNAME = "subghz"
MUSIC_PLAYER_DIRNAME = "music_player"
INFRARED_PLAYER_DIRNAME = "infrared"
DOLPHIN_DIRNAME = "dolphin"
EXTERNAL_PATH = lambda repo: Path("external") / repo


def copy_badusb(src: Path, dst: Path) -> None:
    logging.info("Copy BadUSB assets")
    src /= BADUSB_PLAYER_DIRNAME
    dst /= BADUSB_PLAYER_DIRNAME

    if not src.exists():
        raise FileNotFoundError(str(src))

    if not src.is_dir():
        raise NotADirectoryError(str(src))

    shutil.copytree(src, dst)


def copy_subghz(src: Path, dst: Path) -> None:
    logging.info("Copy SubGHz assets")
    src /= SUBGHZ_PLAYER_DIRNAME
    dst /= SUBGHZ_PLAYER_DIRNAME

    if not src.exists():
        raise FileNotFoundError(str(src))

    if not src.is_dir():
        raise NotADirectoryError(str(src))

    shutil.copytree(src, dst)


def copy_infrared(src: Path, dst: Path) -> None:
    logging.info("Copy InfraRed assets")
    src /= INFRARED_PLAYER_DIRNAME
    dst /= INFRARED_PLAYER_DIRNAME

    if not src.exists():
        raise FileNotFoundError(str(src))

    if not src.is_dir():
        raise NotADirectoryError(str(src))

    shutil.copytree(src, dst)


def copy_music_player(src: Path, dst: Path) -> None:
    logging.info("Copy Music Player assets")
    src /= MUSIC_PLAYER_DIRNAME
    dst /= MUSIC_PLAYER_DIRNAME

    if not src.exists():
        raise FileNotFoundError(str(src))

    if not src.is_dir():
        raise NotADirectoryError(str(src))

    shutil.copytree(src, dst)


def copy_from_githubcom_UberGuidoZ_Flipper(src_root: Path, dst_root: Path) -> None:
    logging.info("Copy from github.com/UberGuidoZ/Flipper")
    src_root /= EXTERNAL_PATH("github.com/UberGuidoZ/Flipper")

    for src_name, dst_name in [
            ('NFC/HID_iClass', 'nfc/HID_iClass'),
            ('NFC/mf_classic_dict', 'nfc/mf_classic_dict'),
            ('Sub-GHz', 'subghz/ugz'),
    ]:
        src = src_root / src_name
        dst = dst_root / dst_name
        if not src.exists():
            raise FileNotFoundError(str(src))

        if not src.is_dir():
            raise NotADirectoryError(str(src))

        shutil.copytree(src, dst)


def copy_from_githubcom_tobiabocchi_flipperzero_bruteforce(src_root: Path, dst_root: Path) -> None:
    logging.info("Copy from github.com/tobiabocchi/flipperzero-bruteforce")
    src_root /= EXTERNAL_PATH("github.com/tobiabocchi/flipperzero-bruteforce")

    for src_name, dst_name in [
            ('sub_files', 'subghz/tfb'),
    ]:
        src = src_root / src_name
        dst = dst_root / dst_name
        if not src.exists():
            raise FileNotFoundError(str(src))

        if not src.is_dir():
            raise NotADirectoryError(str(src))

        shutil.copytree(src, dst)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("src", type=Path, help="Source of assets")
    parser.add_argument("dst", type=Path, help="Destination of compiled assets")
    args = parser.parse_args()

    src = args.src.resolve()
    dst = args.dst.resolve()

    if not src.exists():
        raise FileNotFoundError(str(src))

    if not src.is_dir():
        raise NotADirectoryError(str(src))

    if dst.exists():
        if not dst.is_dir():
            raise NotADirectoryError(str(dst))
        shutil.rmtree(str(dst))
    os.makedirs(str(dst))

    Main(no_exit=True)(["dolphin", str(src / DOLPHIN_DIRNAME), str(dst / DOLPHIN_DIRNAME)])
    copy_music_player(src, dst)
    copy_infrared(src, dst)
    copy_badusb(src, dst)
    copy_subghz(src, dst)
    copy_from_githubcom_UberGuidoZ_Flipper(src, dst)
    copy_from_githubcom_tobiabocchi_flipperzero_bruteforce(src, dst)


if __name__ == '__main__':
    main()
